const express = require('express');

const PORT = 3000;

const app = express();

app.get('/', (req, res) => {
  res.send('Hola mundo desde docker!');
});

app.listen(PORT);

console.log(`Corriendo en el puerto ${PORT}`);