# Usamos version alpine porque es mas liviana
FROM node:12.16.1-alpine3.11

# Creamos directorio de la app
WORKDIR /app

# Copiamos los package* .json
COPY package*.json ./

# Instalamos dependencias adentro del contenedor
RUN npm install

# Copiamos todo el codigo de la aplicación
COPY . .

# Exponemos el puerto 3000
EXPOSE 3000

# Iniciamos la aplicación node
CMD [ "npm", "start" ]
